package com.example.demo.controller;

import com.example.demo.entity.Vacancy;
import com.example.demo.entity.Query;
import com.example.demo.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class VacancyController {

    @Autowired
    VacancyService vacancyService;

    @GetMapping("/vacancy")
    public List<Vacancy> index(Query query) {
        return vacancyService.parseVacancies(query);
    }
}
