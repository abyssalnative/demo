package com.example.demo.entity;

public class Vacancy {

    private String title;
    private String specialization;
    private String companyName;
    private String occupation;
    private String salary;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getTitle() {
        return title;
    }

    public String getSpecialization() {
        return specialization;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getSalary() {
        return salary;
    }
}
