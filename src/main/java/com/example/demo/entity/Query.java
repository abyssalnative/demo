package com.example.demo.entity;

public class Query {

    private String q;
    private String[] divisions;
    private String[] skills;
    private String qid;

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String[] getDivisions() {
        return divisions;
    }

    public void setDivisions(String[] divisions) {
        this.divisions = divisions;
    }

    public String[] getSkills() {
        return skills;
    }

    public void setSkills(String[] skills) {
        this.skills = skills;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }
}
