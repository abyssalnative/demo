package com.example.demo.service;


import com.example.demo.entity.Query;
import com.example.demo.entity.Vacancy;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Service
public class VacancyService {

    private static final String URL = "https://moikrug.ru/vacancies";

    public List<Vacancy> parseVacancies(Query query) {
        String url = "";
        try {
            url = URL + "?" + buildUrlAttributes(query);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return parseVacancies(url);
    }

    private ArrayList<Vacancy> parseVacancies(String url) {
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements jobs = document.select("div.job");
        ArrayList<Vacancy> arrayVacancy = new ArrayList<>();
        for (Element element : jobs) {
            Vacancy vacancy = new Vacancy();
            vacancy.setTitle(element.select("div.title").text());
            vacancy.setSpecialization(element.select("div.specialization").text());
            vacancy.setCompanyName(element.select("span.company_name").text());
            vacancy.setOccupation(element.select("span.occupation").text());
            vacancy.setSalary(element.select("div.salary").text());
            arrayVacancy.add(vacancy);
        }
        return arrayVacancy;
    }

    private String buildUrlAttributes(Query query) throws IllegalAccessException {
        Multimap<String, String> parts = ArrayListMultimap.create();
        Field[] fields = query.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Object fieldValue = field.get(query);
            if (fieldValue == null) {
                continue;
            }
            if (field.getType().equals(String[].class)) {
                for (String value : (String[]) field.get(query)) {
                    parts.put(field.getName() + "[]", value);
                }
            } else {
                parts.put(field.getName(), (String) field.get(query));
            }
        }
        List<String> attributes = new ArrayList<>();
        parts.forEach((key, val) -> attributes.add(key + "=" + val));
        return String.join("&", attributes);
    }

}